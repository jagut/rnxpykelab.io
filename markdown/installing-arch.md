---
title: Installing Archlinux
author: rnxpyke
published: 2019-10-05 17:00:00
---

# Installing Archlinux

I've decided to reinstall Arch Linux on my laptop.  
NixOS has been the daily driver on my laptop for around half a year. Even though the declarative model of NixOS is quite nice, it's not what I am used to. On my desktop I run Arch and it is somewhat inconvenient to figure out how to configure nix on my laptop when I need everything to "just work".

This wouldn't really be an issue normally, because I've done quite a few installs already. But to make matters worse, I've decided to do something new this time around. At first, the goal was just to setup full disk encryption. But then I got into an adventurous mood...

## Boot that machine

Beginning the install process isn't that hard if you know what you are doing. Download the arch iso, verify the checksum and choose a USB Stick that is ready to be _**destroyed**_.  ... Ahem, I mean _formated_. Just make sure that you choose the correct output file when you `dd`[^1] the iso to the stick. Plug in the stick into the laptop, boot the machine and select the correct boot media in the BIOS. Voilà, you are now running Arch!

[^1]: Rumor has it that `dd` stands for copy & convert, but Ken Thompson has already developed a C Compiler with the name cc, so they decided to use the next letter in the alphabet instead.

## Arch Wiki to the rescue!

Sadly, formatting the hard drive is basically the first step in the [installation](https://wiki.archlinux.org/index.php/Installation_guide#Partition_the_disks) after connecting the machine to the network. This means that I'm stepping into unknown territory almost from the beginning. Fortunately, the [arch wiki](https://wiki.archlinux.org) is very detailed and has exactly what I'm looking for:
[Encrypting an entire system](https://wiki.archlinux.org/index.php/Dm-crypt/Encrypting_an_entire_system).
I have a 256GB SSD installed, and I don't plan on changing the configuration anytime soon, so the LVM on LUKS setup fits perfectly for me. LUKS handles the encryption of my SSD. LVM Manages Logical Volumes.  

LVM makes it possible to refer to partitions by name, resize them, and span them over multiple physical disks. Although I won't use the last feature because I only have one hard drive installed, the other two are quite convenient. I can allocate small partitions for my home and root directories at first, and decide how I want to split up the remaining space when these are getting full.

Creating the encrypted partition is rather easy, you just use `cryptsetup` to setup and mount the partition. Inside the encrypted partition I created one LVM volume group named "arch" containing three logical volumes: "root" and "home" and "swap". I then mounted everything to the respective places, including the EFI boot partition that was still there from my previous install. Now you continue with the normal installation procedure.[^2]

[^2]: Quite teasing if you ask me, because you
    only know if you setup everything correctly **after** configuring the bootloader, which is basically the last step of the installation.

I wont go into detail about that, because the arch wiki does a good job in describing the normal installation procedure. Off the top of my head it would just be pacstraping the packages from the ISO to the mount point of the new installation root, configuring the hostname, generating the locale and an fstab as well as setting installing important network packages on the new root partition. [^3]  

[^3]: Don't forget this step. Just like I did. Or you'll find yourself booting into the Arch ISO for the fifth time because you just _forgot_ something crucial. Like the wireless network setup...  

Once that is all done it's time to install the bootloader. It's crucial to not forget that encrypted partition I made in the beginning. Everything that is needed to decrypt that has to be accessible to the bootloader. That includes the kernel and the dmcrypt software that handles the en/decryption itself. They are all stored in the unencrypted boot partition.

To make sure they are included you have to change the initramfs hooks and configure kernel boot parameters so the kernel knows where the root file system is stored. In my case I just had to setup the encrypt keyboard and keymap hooks in `/etc/mkinitcpio.conf` and configure the crpytdevice root and swap kernel parameters in the `/etc/default/grub` config.  Don't forget to generate the new configuration with `mkinitramfs` and `grub-mkconfig` respectively.

## Reboot that machine

Now comes the moment of truth.  
Turning the power off, then on again.  
The familiar BIOS splash screen greets me.  Then the plain GRUB menu shows up.  
The entry for my arch install is there.
I press enter, hoping for the best.

First, nothing.

Then, the mighty Linux kernel shows up.

...

...

In a rescue shell.

"Dammit" I think to myself as I see a `couldn't find root file system` warning taunting me. I begin checking  out which commands are available to me in this unfamiliar environment. A simple `ls` reveals the normal root file system layout. I form a hypothesis in my mind. Maybe the encryption routines aren't available to the kernel yet. I begin typing the keys that would enlighten me.
`l`. `s`. `/`. `b`. `i`. `n`.
The normal POSIX commands appear spread across multiple columns, neatly ordered in a lexicographical fashion. I scan the columns for the magical command.
`c`... `c`.. `c`.
There it is! `cryptsetup`. The utility to mount the secure encrypted partition. I type the commands that I used the first time around to mount my new home. And to my surprise, everything works! I type the Ctrl+D key Combination in frustration. I have no idea why the boot process didn't work.
I type Ctrl+D again.

The light of my caps lock key begins blinking.  
The words `kernel panic!` flash across the screen.  
Then, blackness.

Defeat.

In my frustration I begin to look up how others have configured their systems on the forbidden website. I search for an appropriate video, set the playback speed to x2 and watch the installation processes. Skipping the parts that I'm familiar with I am looking for the one stupid thing I forgot.

Because it is always one stupid thing.  

And this time, it was the grub setting `GRUB_ENABLE_CRUPTODISK`. I reboot into the Arch ISO, and mount the partitions. chroot into the installation. Uncomment that one stupid line in the config file that isn't enabled by default. Regenerate the boot partition with that one command that takes way to many arguments because I'm using a x86_64 EFI.

I reboot. Again. A sense of accomplishment washes over me as the expected boot process unfolds. I am now asked for my cryptsetup passphrase on boot, which means that I have a full disk encrypted installation working!

That is, as long as you consider a tty shell as a working installation.
