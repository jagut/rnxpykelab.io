#!/usr/bin/env python3

import frontmatter
import sys
import re
from datetime import datetime

# <div class=post>
# <p class=title>$Title$</p>
# <p class=author>$Author$</p>
# <
# </div>
#
#

# (markdown)/(=name.*)(.md)
# /article/(name).html

def header():
    return("""
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" type="text/css" href="styles.css" />
    </head>
    <body class="wrapper">
        <div class="nav">
            <h1>Bonsaibeef</h1>
        </div>
    
""")


def footer():
    return("""
    </body>
</html>
""");

def htmlize(path, post):
    published = datetime.strftime(post['published'], '%Y-%m-%d')
    pattern = r"^markdown/(.*).md$"
    match = re.match(pattern, path)
    if match:
        path =  f'article/{match.group(1)}.html'
    else:
        raise ValueError(f"the post path does not match the '{pattern}' regex")
    return (f"""
<div class=post>
    <a href=\"{path}\" class=title>{post['title']}</a>
    <p class=author>{post['author']}</p>
    <p class=date>{published}</p>
</div>
    """)

def main():
    files = []
    for file in sys.argv[1:]:
        files.append(file)
    
    data = { file: frontmatter.load(file).metadata for file in files }
    
    print(header())
    
    for path, meta in sorted(data.items(), key=lambda item: item[1]['published'], reverse=True):
        print(htmlize(path,meta))

    print(footer())

if __name__ == "__main__":
    main()
