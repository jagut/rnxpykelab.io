# Bonsaibeef

## About

This is my personal blog, hosted on *Gitlab Pages* at https://rnxpyke.gitlab.io


## How to build

Basically all content is written in Markdown and transpiled into static html files. The Makefile builds the entire blog in the `public/` directory.

On every commit on master the CI/CD is triggered to deploy the current version. To check out which tools are needed, look at the `.gitlab-ci.yml` file for the most acurate build information.

To learn more about the build process, read the *Build your Blog the Hard Way* Post.

## Contributing

Basically anyone could at their own markdown formatted post, my build process only requires the YAML frontmatter with the `title, author` and `published` keys. See the existing posts for expected formatting. If you should contribute is another question.

Fixing typos, CSS or other things in my build process is very much appreciated.
