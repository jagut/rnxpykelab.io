SOURCES := $(shell cd markdown; find -type f -name "*.md")
ARTDIR := public/article
ARTICLES := $(addprefix $(ARTDIR)/,$(SOURCES:.md=.html))

all: public/index.html public/styles.css public/article $(ARTICLES) 


public/styles.css: stubs/styles.css
	cp $< $@

public/index.html: $(addprefix markdown/,$(SOURCES))
	mkdir -p public
	./scripts/index.py $^ > $@
	

public/article: public
	mkdir -p $(ARTDIR)

public: 
	mkdir -p public 
	cp stubs/styles.css public/

public/article/%.html: markdown/%.md
	./scripts/frontdown -f footnotes < $< | ./scripts/glue.sh /dev/stdin > $@

%PHONY: clean public

clean:
	rm -rf public
